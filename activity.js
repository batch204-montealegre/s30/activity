//NUMBER 2

db.fruits.aggregate([
        {$match:{"onSale": true}},
        {$count: "totalNumberofFruits_onSale"}])

//NUMBER 3


db.fruits.aggregate([
        {$match:{"stocks":{$gt: 20}}},
        {$count: "fruits_MoreThan20_Stocks"}])

//NUMBER 4

db.fruits.aggregate([
        {$match:{"onSale": true}},
        {$group: {_id: "$supplier", avgPrice: { $avg: "$price"}}}
])

//NUMBER 5

db.fruits.aggregate([
        {$group: {_id: "$supplier", maxPrice: { $max: "$price"}}}
]);

//NUMBER 6

db.fruits.aggregate([
   
        {$group: {_id: "$supplier", minPrice: { $min: "$price"}}}
]);